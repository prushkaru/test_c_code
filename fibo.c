#include <stdio.h>
#include <gmp.h>
#include <time.h>
#define TARGET_NUM 1000000

void fibo(int tn);

mpz_t result,next,prev,cur;

int main() {
struct timespec ts;
mpz_init(result);
mpz_init_set_ui (cur,1);
mpz_init(next);
mpz_init(prev);
long start,stop,start_sec,stop_sec;
double whole_time;

timespec_get(&ts, TIME_UTC);
start  = ts.tv_nsec;
start_sec = ts.tv_sec;

fibo(TARGET_NUM);

timespec_get(&ts, TIME_UTC);
stop   = ts.tv_nsec;
stop_sec = ts.tv_sec;

gmp_printf("Fibo #%d = %Zd\n",TARGET_NUM,result);

whole_time = stop_sec-start_sec;
whole_time += (stop-start)/1000000000.0;
printf("Whole time = %.2fs\n",whole_time);

return(0);
}

void fibo(int tn) {
        if (tn==0) {
                mpz_set_ui(result,0);
                return;
        }
        if (tn<=2) {
                mpz_set_ui(result,1);
                return;
        }

        while(tn-->1) {
                mpz_add(next,cur,prev);
                mpz_set(prev,cur);
                mpz_set(cur,next);
        }
        mpz_set(result,cur);
}

